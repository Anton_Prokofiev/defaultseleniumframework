﻿using NUnit.Framework;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DefaultSeleniumFramework.BaseClases;


namespace DefaultSeleniumFramework.Tests
{
    [Category("test")]
    class GoogleTest : BaseTest
    {
        public override string GetAccessToSite()
        {
            return "";
        }

        public override string GetEnvUrl(Env env = Env.Dev)
        {
            return "https://www.google.com/";
        }

        [Test(Description =
                "Example Google test")]
        [TestCase("FireFox")]
        public void GoogleTestExample(string BrowserName)
        {
            RemoteWebDriver driver = DriverSetUp();
            driver.Navigate().GoToUrl("https://www.google.com/");

            GooglePage googlePage = new GooglePage(driver);
            googlePage.InputInSearchField("Test search");
        }
        [Test(Description =
                "Example Google test")]
        [TestCase("Chrome")]
        public void GoogleTestExample2(string BrowserName)
        {
            RemoteWebDriver driver = DriverSetUp(BrowserName);

            driver.Navigate().GoToUrl("https://www.google.com/");
            GooglePage googlePage = new GooglePage(driver);
            googlePage.InputInSearchField("Test search");
        }
    }
}
