﻿using DefaultSeleniumFramework.Helpers.WaitHelpers;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DefaultSeleniumFramework.Helpers
{
   public static class MainHelper
    {
        public static IWebElement S(this IWebDriver driver, String XPath, string errorMsg = "Element is not visible", int waitSec = 10)
        {
            return driver.waitForElementIsVisible(By.XPath(XPath), errorMsg, waitSec);
        }
        /// <summary>
        /// Find element and wait for element will be visible
        /// </summary>
        /// <param name="locator"> By locator By.XPath| By.CSS ... </param>
        public static IWebElement S(this IWebDriver driver, By locator, string errorMsg = "Element is not visible", int waitSec = 10)
        {
            return driver.waitForElementIsVisible(locator, errorMsg, waitSec);
        }
    }
}
