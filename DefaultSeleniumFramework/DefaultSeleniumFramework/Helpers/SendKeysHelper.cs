﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DefaultSeleniumFramework.Helpers
{
    public static class SendKeysHelper
    {
        public const int WAIT_SEC = 10;
        /// <summary>
        /// Clears the content of this element.
        /// </summary>
        /// <param name="errMsg"> error message </param>
        /// <param name="WaitSec"> waitTime in sec </param>
        public static IWebElement clear(this IWebElement element, String errMsg = "can't clear field of element", int WaitSec = WAIT_SEC)
        {
            try
            {
                element.Clear();
            }
            catch (NoSuchElementException e)
            {
                Assert.Fail(errMsg + " | " + e.Message, e);
            }
            catch (WebDriverException e)
            {
                Assert.Fail(errMsg + " | " + e.Message);
            }
            finally
            {
            }
            return element;
        }

        /// <summary>
        /// Simulates typing text into the element.
        /// </summary>
        /// <param name="text">
        /// The text to type into the element.
        /// </param>
        /// <param name="errMsg"> error message </param>
        /// <param name="WaitSec"> waitTime in sec </param>
        public static IWebElement sendKeys(this IWebElement element, String text, String errMsg = "can't sendKeys in field of element", int WaitSec = WAIT_SEC)
        {
            try
            {
                element.SendKeys(text);
            }

            catch (NoSuchElementException e)
            {
                Assert.Fail(errMsg + " | " + e.Message, e);
            }
            catch (WebDriverException e)
            {
                Assert.Fail(errMsg + " | " + e.Message);
            }
            finally
            {
            }
            return element;
        }

        /// <summary>
        /// Simulates typing text into the element.
        /// </summary>
        /// <param name="text">
        /// The text to type into the element.
        /// </param>
        /// <param name="errMsg"> error message </param>
        /// <param name="WaitSec"> waitTime in sec </param>
        public static IWebElement clearAndFill(this IWebElement element, String text, String errMsg = "can't clear and sendKeys in field of element", int WaitSec = WAIT_SEC)
        {
            try
            {
                element.clear().sendKeys(text);
            }
                catch (NoSuchElementException e)
            {
                Assert.Fail(errMsg + " | " + e.Message, e);
            }
            catch (WebDriverException e)
            {
                Assert.Fail(errMsg + " | " + e.Message);
            }
            finally
            {
            }
            return element;
        }
    }
}
