﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace DefaultSeleniumFramework.Helpers.WaitHelpers
{
    public static class WaitHelpers
    {
        public const int WAIT_SEC = 10;

        /// <summary>
        /// Waiting until element will be clickable
        /// </summary>
        /// <param name="errMsg"> error message </param>
        /// <param name="WaitSec"> waitTime in sec </param>
        /// <summary>
        public static IWebElement waitUntilElementIsClickable(this IWebDriver driver, By locator, int waitSec = WAIT_SEC, String ErrorMsg = "this element isn't clickable")
        {
            new WebDriverWait(driver, TimeSpan.FromSeconds(waitSec)).Until(isElementClickable(locator));
            return driver.FindElement(locator);
        }

        public static IWebElement waitForElementIsVisible(this IWebDriver driver, By locator,
            String ErrorMsg = "this element isn't visible", int waitSec = WAIT_SEC)
        {
            try
            {
                new WebDriverWait(driver, TimeSpan.FromSeconds(waitSec)).Until(isElementDisplayed(locator));
            }
            catch (WebDriverTimeoutException err)
            {
                try
                {
                    driver.FindElement(locator);
                }
                catch (NoSuchElementException e)
                {
                    Assert.Fail("No such element" + " | " + e.Message);
                }
                Assert.Fail(ErrorMsg + " | " + err.Message);
            }
            return driver.FindElement(locator);
        }

        /// <summary>
        /// Waiting until element will be clickable
        /// </summary>
        /// <param name="errMsg"> error message </param>
        /// <param name="WaitSec"> waitTime in sec </param>
        /// <summary>
        public static IWebElement waitUntilElementIsClickable(this IWebElement element, IWebDriver driver, int waitSec = WAIT_SEC, String ErrorMsg = "this element still isn't clickable")
        {
            new WebDriverWait(driver, TimeSpan.FromSeconds(waitSec)).Until(isElementClickable(element));
            return element;
        }


        /// <summary>
        /// Waiting for text of element will not empty
        /// </summary>
        /// <param name="errMsg"> error message </param>
        /// <param name="WaitSec"> waitTime in sec </param>
        /// <summary>
        public static IWebElement waitForElementTextIsNotEmpty(this IWebDriver driver, By locator, int waitSec = WAIT_SEC, String ErrorMsg = "this element still hasn't text")
        {
            new WebDriverWait(driver, TimeSpan.FromSeconds(waitSec)).Until(isTextInElementNotEmpty(locator));

            return driver.FindElement(locator);

        }

        /// <summary>
        /// Waiting for text of element will not empty
        /// </summary>
        /// <param name="errMsg"> error message </param>
        /// <param name="WaitSec"> waitTime in sec </param>
        /// <summary>
        public static IWebElement waitForElementTextIsNotEmpty(this IWebElement element, IWebDriver driver, int waitSec = WAIT_SEC, String ErrorMsg = "this element still hasn't text")
        {

            new WebDriverWait(driver, TimeSpan.FromSeconds(waitSec)).Until(isTextInElementNotEmpty(element));
            return element;
        }


        //!  ===========  PRIVATE WAITERS ========
        // return bool is elementDisplayed(for waiters)
        private static Func<IWebDriver, bool> isElementDisplayed(By locator)
        {
            return driver =>
            {
                try
                {
                    return driver.FindElement(locator).Displayed;
                }
                catch (NoSuchElementException e)
                {
                    // If element is null, stale or if it cannot be located
                    return false;
                }
            };
        }

        // return bool is element clickable(for waiters)
        private static Func<IWebDriver, bool> isElementClickable(By locator)
        {
            return driver =>
            {
                try
                {
                    IWebElement element = driver.FindElement(locator);
                    return (element.Displayed && element.Enabled);
                }
                catch (NoSuchElementException e)
                {
                    // If element is null, stale or if it cannot be located
                    return false;
                }
                catch (ElementNotInteractableException e)
                {
                    return false;
                }
            };
        }

        private static Func<IWebDriver, bool> isElementClickable(IWebElement element)
        {
            return driver =>
            {
                try
                {
                    //IWebElement element = driver.FindElement(locator);
                    return (element.Displayed && element.Enabled);
                }
                catch (NoSuchElementException e)
                {
                    // If element is null, stale or if it cannot be located
                    return false;
                }
                catch (ElementNotInteractableException e)
                {
                    return false;
                }
            };

        }

        private static Func<IWebDriver, bool> isTextInElementNotEmpty(IWebElement element)
        {
            return driver =>
            {
                try
                {
                    //IWebElement element = driver.FindElement(locator);
                    Boolean isNotEmpty = element.Text != "";
                    return isNotEmpty;
                }
                catch (NoSuchElementException e)
                {
                    // If element is null, stale or if it cannot be located
                    return false;
                }
                catch (ElementNotInteractableException e)
                {
                    return false;
                }
            };

        }

        private static Func<IWebDriver, bool> isTextInElementNotEmpty(By locator)
        {
            return driver =>
            {
                try
                {
                    IWebElement element = driver.FindElement(locator);
                    return (element.Text != "");
                }
                catch (NoSuchElementException e)
                {
                    // If element is null, stale or if it cannot be located
                    return false;
                }
                catch (ElementNotInteractableException e)
                {
                    return false;
                }
            };
        }
    }
}
