﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DefaultSeleniumFramework.Helpers.WaitHelpers;
using OpenQA.Selenium.Remote;

namespace DefaultSeleniumFramework.Helpers
{
    public static class ClickHelper
    {
        public const int CLICKABLE_WAIT_SEC = 10;
        /// <summary>
        /// Default click on element, without waiters
        /// </summary>
        /// <param name="errMsg"> error message </param>
        /// <param name="WaitSec"> waitTime in sec </param>
        public static IWebElement clickOnElement(this IWebElement element, String errMsg = "can't click on element")
        {

            try
            {
                element.Click();
            }

            catch (NoSuchElementException e)
            {
                Assert.Fail(errMsg + " | " + e.Message, e);
            }
            catch (ElementNotInteractableException e)
            {
                Assert.Fail(errMsg + " | " + e.Message, e);
            }
            catch (WebDriverException e)
            {
                Assert.Fail(errMsg + " | " + e.Message, e);
            }
            finally
            {
            }
            return element;
        }
        /// <summary>
        /// Click on element, if click catch error - selenium clicked by js
        /// </summary>
        /// <param name="errMsg"> error message </param>
        /// <param name="WaitSec"> waitTime in sec </param>
        public static IWebElement clickOnElementWithJS(this IWebElement element, RemoteWebDriver driver, String errMsg = "can't click on element")
        {
            try
            {
                element.Click();
            }

            catch (NoSuchElementException e)
            {
                Assert.Fail(errMsg + " | " + e.Message, e);
            }
            catch (ElementNotInteractableException e)
            {
                Assert.Fail(errMsg + " | " + e.Message, e);

            }
            catch (WebDriverException e)
            {
                try
                {
                    IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                    js.ExecuteScript("arguments[0].click();", element);
                }
                catch (WebDriverException err)
                {
                    Assert.Fail(errMsg + " | " + e.Message, err);
                }
            }
            finally
            {
            }
            return element;
        }
        /// <summary>
        /// Wait for element will be clickable then Click on element, if click catch error - selenium clicked by js
        /// </summary>
        /// <param name="errMsg"> error message </param>
        /// <param name="WaitSec"> waitTime in sec </param>
        public static IWebElement clickOnElementWithWaitJS(this IWebElement element, RemoteWebDriver driver, String errMsg = "can't click on element", int WaitSec = CLICKABLE_WAIT_SEC)
        {
            element.waitUntilElementIsClickable(driver, WaitSec);
            try
            {
                element.Click();
            }

            catch (NoSuchElementException e)
            {
                Assert.Fail(errMsg + " | " + e.Message, e);
            }
            catch (ElementNotInteractableException e)
            {
                Assert.Fail(errMsg + " | " + e.Message, e);

            }
            catch (WebDriverException e)
            {
                try
                {
                    IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                    js.ExecuteScript("arguments[0].click();", element);
                }
                catch (WebDriverException err)
                {
                    Assert.Fail(errMsg + " | " + e.Message, err);
                }
            }
            finally
            {
            }

            return element;
        }
        /// <summary>
        /// Wait for element will be clickable then Click on element
        /// </summary>
        /// <param name="errMsg"> error message </param>
        /// <param name="WaitSec"> waitTime in sec </param>
        public static IWebElement clickOnElementWithWait(this IWebElement element, RemoteWebDriver driver, String errMsg = "can't click on element", int WaitSec = CLICKABLE_WAIT_SEC)
        {
            element.waitUntilElementIsClickable(driver, WaitSec);
            try
            {
                element.Click();
            }

            catch (NoSuchElementException e)
            {
                Assert.Fail(errMsg + " | " + e.Message, e);
            }
            catch (ElementNotInteractableException e)
            {
                Assert.Fail(errMsg + " | " + e.Message, e);

            }
            catch (WebDriverException e)
            {
                Assert.Fail(errMsg + " | " + e.Message, e);
            }
            finally
            {
            }
            return element;
        }
    }
}
