﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DefaultSeleniumFramework.BaseClases;
using DefaultSeleniumFramework.Helpers.WaitHelpers;
using DefaultSeleniumFramework.Helpers;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;

namespace DefaultSeleniumFramework.Tests
{
    class GooglePage : BasePage
    {
        public GooglePage(RemoteWebDriver driver) : base(driver)
        {
        }

        public void InputInSearchField(String searchQuery)
        {
            S("//input[@name='q']").clear().sendKeys("Automation testing");
            String str = S("//div[@id='SIvCob']").waitForElementTextIsNotEmpty(driver).Text;
            Console.WriteLine(str);
            S("(//input[@name='btnK'])[1]").clickOnElement();
        }
    }
}
