﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using NUnit.Framework;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using System.Configuration;
using OpenQA.Selenium.Remote;
using DefaultSeleniumFramework.Helpers;

namespace DefaultSeleniumFramework.BaseClases
{
    public class BasePage
    {
        public const int WAIT_SEC = 10;
        protected int DefaultWaitTime = 10;
        public RemoteWebDriver driver { get; private set; }
        public BasePage(RemoteWebDriver driver)
        {
            this.driver = driver;
        }
        /// <summary>
        /// Find element and wait for element will be visible
        /// </summary>
        /// <param name="XPath"> XPath locator </param>
        public IWebElement S(String XPath)
        {
            return driver.S(XPath);
        }
        /// <summary>
        /// Find element and wait for element will be visible
        /// </summary>
        /// <param name="locator"> By locator By.XPath| By.CSS ... </param>
        protected IWebElement S(By locator)
        {
            //By locator = By.XPath(XPath);
            //IWebElement element = driver.waitForElementIsVisible(locator, 10);
            return driver.S(locator);
        }
        public String getValueFromAppConfig(String varName)
        {
            String appVar = ConfigurationManager.AppSettings[varName];
            return appVar;
        }
        public IWebElement getElement(By locator, String errMsg)
        {
            IWebElement webElement = driver.FindElement(locator);
            return webElement;
        }

        public ReadOnlyCollection<IWebElement> getElements(By locator, String errMsg)
        {
            try
            {
                ReadOnlyCollection<IWebElement> webElements = driver.FindElements(locator);
                return webElements;
            }
            catch (NoSuchElementException e)
            {
                Assert.Fail(errMsg + " | " + e.Message, e);
                return null;
            }
        }

        protected int getElementsCount(By locator, String errMsg)
        {
            ReadOnlyCollection<IWebElement> webElements = getElements(locator, errMsg);
            return webElements.Count;
        }

        protected void isDisplayed(By locator, int waitSec, String errMsg)
        {
            try
            {
                //IWebElement element = GetElement(locator, errMsg);
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(waitSec));
                wait.Until(ExpectedConditions.ElementIsVisible(locator));

                Assert.That(driver.FindElement(locator).Displayed, errMsg);
            }
            catch (NoSuchElementException e)
            {
                Assert.Fail(errMsg + " | " + e.Message, e);
            }
        }
        protected void lazyClickOnElement(By locator, int waitSec, String errMsg)
        {
            try
            {
                Lazy<IWebElement> element = new Lazy<IWebElement>(() => getElement(locator, errMsg));
                //IWebElement element = GetElement(locator, errMsg);
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(waitSec));
                wait.Until(ExpectedConditions.ElementIsVisible(locator));

                Assert.That(element.Value.Enabled, errMsg);
                element.Value.Click();
            }
            catch (WebDriverException e)
            {
                Assert.Fail(errMsg + " | " + e.Message, e);
            }
        }
        protected void clickOnElement(By locator, int waitSec, String errMsg)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(waitSec));
            wait.Until(ExpectedConditions.ElementToBeClickable(locator));
            IWebElement element = getElement(locator, errMsg);
            Assert.That(element.Enabled, errMsg);
            try
            {
                element.Click();
            }
            catch (NoSuchElementException e)
            {
                Assert.Fail(errMsg + " | " + e.Message, e);
            }
            catch (ElementNotInteractableException e)
            {
                Assert.Fail(errMsg + " | " + e.Message, e);
            }
            catch (WebDriverException e)
            {
                try
                {
                    IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                    js.ExecuteScript("arguments[0].click();", element);
                }
                catch (WebDriverException err)
                {
                    Assert.Fail(errMsg + " | " + e.Message, err);
                }
            }
            finally
            {
            }
        }

        protected void clearAndFillField(By locator, String inputText, int waitSec, String errMsg)
        {
            try
            {
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(waitSec));
                wait.Until(ExpectedConditions.ElementIsVisible(locator));
                IWebElement element = getElement(locator, errMsg);

                Assert.That(element.Displayed, errMsg);

                element.Clear();
                element.SendKeys(inputText);
            }
            catch (WebDriverException e)
            {
                Assert.Fail(errMsg + " | " + e.Message, e);
            }
            finally
            {
            }
        }
        protected String getTextFromElement(By locator, int waitSec, String errMsg)
        {
            try
            {
                WebDriverWait waitUntilVisible = new WebDriverWait(driver, TimeSpan.FromSeconds(waitSec));
                waitUntilVisible.Until(ExpectedConditions.ElementIsVisible(locator));

                WebDriverWait waitUntilTextAppear = new WebDriverWait(driver, TimeSpan.FromSeconds(waitSec));
                waitUntilTextAppear.Until<bool>(ctx => ctx.FindElement(locator).Text != "");
                IWebElement element = getElement(locator, errMsg);
                return element.Text;
            }
            catch (WebDriverException e)
            {
                Assert.Fail(errMsg + " | " + e.Message, e);
                return "";
            }
        }
        public int randomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }
    }
}
