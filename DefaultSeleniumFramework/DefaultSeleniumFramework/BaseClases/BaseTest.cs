﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DefaultSeleniumFramework.Helpers;

namespace DefaultSeleniumFramework.BaseClases
{
    [TestFixture]
    public abstract class BaseTest
    {
        public abstract String GetAccessToSite();
        public enum Env { Dev, Stage }
        public Env TestedEnv { get; private set; }
        public void SetTestedEnv(Env env)
        {
            TestedEnv = env;
        }
        public string myValue { get; set; }
        public String SITE_URL;
        public abstract String GetEnvUrl(Env env = Env.Dev);


        private RemoteWebDriver driver;
        private Boolean IsLocalRunning = true;
        private String SelenoidUrl = "http://192.168.44.128";
        private String BrowserType = "chrome";
        private String BrowserVersion = "72.0";

        public RemoteWebDriver GetDriver()
        {
            return this.driver;
        }
        public void SetDriver(RemoteWebDriver driver)
        {
            if (driver == null)
            {
                throw new ArgumentNullException(nameof(driver));
            }

            this.driver = driver;

        }



        public RemoteWebDriver DriverSetUp(String browserName = "Chrome")
        {
            RemoteWebDriver driver;

            Console.WriteLine("BrowserName = " + browserName);
            if (this.IsLocalRunning)
            {

                switch (browserName)
                {
                    case "Chrome":
                        driver = new ChromeDriver();
                        SetDriver(driver);
                        break;
                    case "Firefox":
                        driver = new FirefoxDriver();
                        SetDriver(driver);
                        break;
                    default:
                        driver = new ChromeDriver();
                        break;
                }
                SetBrowserWindowResolution();
                return driver;

            }
            else
            {
                var capabilities = new DesiredCapabilities(BrowserType, BrowserVersion, new Platform(PlatformType.Any));
                capabilities.SetCapability("screenResolution", "1920x1080");
                //capabilities.SetCapability("enableVNC", false);
                driver = new RemoteWebDriver(new Uri(SelenoidUrl + ":7777/wd/hub/"), capabilities);
                SetDriver(driver);

                SetBrowserWindowResolution();
                return driver;
            }


        }
        private void SetBrowserWindowResolution(int x = 1366, int y = 1024)
        {
            GetDriver().Manage().Window.Size = new Size(x, y);
        }

        [TearDown]
        public void TearDown()
        {
            String testStr = this.GetType().Attributes.ToString();
            String className = this.GetType().Name;
            string imagesDirectory = AppDomain.CurrentDomain.BaseDirectory + "Images\\";
            String Date = DateTime.Today.ToString("dd-MM-yyyy") + "-" + DateTime.Now.ToString("HH-mm-ss");

            string imageName = className + "-" + Date + ".png";

            // use reflection to find all tests in the fixture
            var tests = this.GetType()
                        .GetMethods()
                        .Where(method => method.GetCustomAttributes(typeof(TestAttribute), false).Count() > 0);
            Console.WriteLine(tests);

            try
            {
                Directory.CreateDirectory(imagesDirectory);
                ((ITakesScreenshot)GetDriver()).GetScreenshot().SaveAsFile(imagesDirectory + imageName, ScreenshotImageFormat.Png);
            }
            catch (IOException ioex)
            {
                Console.WriteLine(ioex.Message);
            }



            GetDriver().Quit();
        }
        /// <summary>
        /// Find element and wait for element will be visible
        /// </summary>
        /// <param name="XPath"> XPath locator </param>
        protected IWebElement S(String XPath)
        {
            return driver.S(XPath);
        }
        public String getFromAppConfig(String varName)
        {
            String appVar = ConfigurationManager.AppSettings[varName];

            return appVar;
        }
    }
}
